require('dotenv').config();
const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser')
const userRoutes = require('./routes/user');
const emailRoutes = require('./routes/email');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use('/', userRoutes);
app.use('/', emailRoutes);

const start = async () => {

    await mongoose.connect(process.env.DB_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    });

    app.listen(process.env.PORT, () => {
        console.log(`Started`);
    })
}

start().catch((e) => {
    console.log(e)
});