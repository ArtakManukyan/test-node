const { Router } = require('express')
const router = Router()
const userValidator = require('../validators/user')
const user = require('../controllers/user')
const multer  = require('multer')
const upload = multer({ dest: 'uploads/' })

router.get('/users', user.index)
router.get('/users/:id', user.show)
router.post('/users', userValidator, user.store)
router.put('/users/:id', userValidator,  user.update)
router.delete('/users/:id', user.delete)
router.post('/upload/user/data', upload.single('userData'), user.uploadUserData)

module.exports = router