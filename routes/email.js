const { Router } = require('express')
const router = Router()
const email = require('../controllers/email')
const singleValidator = require('../validators/email/single')
const multipleValidator = require('../validators/email/multiple')

router.post('/send/single', singleValidator, email.sendSingleEmail)
router.post('/send/multiple', multipleValidator, email.sendMultipleEmail)
router.post('/send/us', email.sendUsEmails)


module.exports = router