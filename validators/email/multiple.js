const { validationResult } = require('express-validator');
const { body } = require('express-validator');

module.exports = [
    body('emails')
        .not().isEmpty().withMessage('The emails is required.').isArray().withMessage('Invalid emails array.'),

    (req, res, next) => {

        const errors = validationResult(req);

        if (!errors.isEmpty()) {

            return res.status(422).json({ errors: errors.array() });
        }

        next();
    }
]