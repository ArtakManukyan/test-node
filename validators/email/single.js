const { validationResult } = require('express-validator');
const { body } = require('express-validator');

module.exports = [
    body('email')
        .not().isEmpty().withMessage('The email is required.').isEmail().withMessage('Invalid email address.'),

    (req, res, next) => {

        const errors = validationResult(req);

        if (!errors.isEmpty()) {

            return res.status(422).json({ errors: errors.array() });
        }

        next();
    }
]