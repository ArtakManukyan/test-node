const { validationResult } = require('express-validator');
const { body } = require('express-validator');

module.exports = [
    body('username')
        .not().isEmpty().withMessage('The name is required.'),
    body('email')
        .not().isEmpty().withMessage('The email is required.').isEmail().withMessage('Invalid email address.'),
    body('phone')
        .not().isEmpty().withMessage('The phone is required.'),
    body('template_messages')
        .not().isEmpty().withMessage('The template messages is required.')
        .isArray({min:1}).withMessage('you should have minimum one template message.'),

    (req, res, next) => {

        const errors = validationResult(req);

        if (!errors.isEmpty()) {

            return res.status(422).json({ errors: errors.array() });
        }

        next();
    }
]