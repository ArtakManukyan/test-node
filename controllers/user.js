const User = require('../models/user');
const reader = require('xlsx')
const fs = require('fs')

exports.index = async (req, res) => {

    try {
        const users = await User.find();

        res.json({
            data: {
                users
            }
        });
    } catch (e) {
        res.status(400).json({error: 'Oops, Something Went Wrong.'});
    }
}

exports.show = async (req, res) => {

    try {
        const id = req.params.id;

        const user = await User.findById(id);

        res.json({
            data: {
                user
            }
        });
    } catch (e) {
        res.status(400).json({error: 'Oops, Something Went Wrong.'});
    }
}

exports.store = async (req, res) => {

    try {
        const data = {...req.body};

        const user = new User(data);

        await user.save();

        res.json({
            data: {
                user
            }
        });
    } catch (e) {
        res.status(400).json({error: 'Oops, Something Went Wrong.'});
    }
}

exports.update = async (req, res) => {

    try {
        const data = {...req.body};

        const id = req.params.id;

        const user = await User.findByIdAndUpdate(id, data, {new: true});

        res.json({
            data: {
                user
            }
        });
    } catch (e) {
        res.status(400).json({error: 'Oops, Something Went Wrong.'});
    }
}

exports.delete = async (req, res) => {

    try {
        const id = req.params.id;

        await User.findByIdAndDelete(id);

        res.status(204).json({data: {}});
    } catch (e) {
        res.status(400).json({error: 'Oops, Something Went Wrong.'});
    }
}

exports.uploadUserData = async (req, res) => {

    try {
        const filePath = __dirname + `/../uploads/${req.file.filename}`

        const file = await reader.readFile(filePath)

        const data = reader.utils.sheet_to_json(file.Sheets[file.SheetNames[0]])

        const usersArray = data.map((item) => {

            return {
                ...item,
                template_messages: JSON.parse(item.template_messages),
                phone:item['phone number']
            }
        })

        const users = await User.insertMany(usersArray)

        fs.unlinkSync(filePath)

        res.status(201).json({
            data: {
                users
            }
        });
    } catch (e) {
        res.status(400).json({error: 'Oops, Something Went Wrong.'});
    }
}