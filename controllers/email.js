const Email = require('../services/email')
const {phone} = require('phone')
const User = require("../models/user")
exports.sendSingleEmail  = async (req, res) => {

    try {
        const email = new Email([req.body.email], 'Single Email','Single Email','Single Email')

        await email.send()

        res.status(204).json();
    } catch (e) {
        res.status(400).json({error: 'Oops, Something Went Wrong.'});
    }
}

exports.sendMultipleEmail  = async (req, res) => {

    try {
        const email = new Email(req.body.emails, 'Multiple Email','Multiple Email','Multiple Email')

        await email.send()

        res.status(204).json()
    } catch (e) {
        res.status(400).json({error: 'Oops, Something Went Wrong.'})
    }
}

exports.sendUsEmails  = async (req, res) => {

    try {
        const users = await User.find();

        const usUsers = users.filter((item) => {
            const phoneData = phone(item.phone.toString())
            return phoneData.countryIso2 === "US"
        })

        const usUsersEmail = usUsers.map((item) => {
            return item.email
        })

        if (usUsersEmail.length){

            const email = new Email(usUsersEmail, 'US Email','US Email','US Email')

            await email.send()
        }

        res.status(204).json();
    } catch (e) {
        console.log(e)
        res.status(400).json({error: 'Oops, Something Went Wrong.'})
    }
}