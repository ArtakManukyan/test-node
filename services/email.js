require('dotenv').config();
const sendGridMail = require('@sendgrid/mail');
sendGridMail.setApiKey(process.env.SENDGRID_API_KEY);

class Email {


    constructor(to, subject, text, html) {
        this.msg = {
            to,
            from: 'amanukyan@technologist.ai',
            subject,
            text,
            html,
        };
    }

    async send() {
        await sendGridMail.send(this.msg)
    }
}

module.exports = Email